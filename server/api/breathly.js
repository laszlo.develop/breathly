const { RESTDataSource } = require('apollo-datasource-rest');

class BreathlyAPI extends RESTDataSource {
  responseStatuses;

  constructor() {
    super();
    this.baseURL = 'https://api.saveecobot.com/output.json';
    this.responseStatuses = {
      success: true,
    };
  }

  async getAllSupportedCities() {
    const response = await this.get('');

    return Array.isArray(response)
      ? [...new Set(response.map(cityAirInfo => cityAirInfo.cityName))]
      : [];
  }

  async getCitesInfo() {
    const response = await this.get('');

    return Array.isArray(response) ?
      response.map(cityInfo => ({
      id: cityInfo.id,
      name: cityInfo.cityName,
      airQualityIndex: cityInfo.pollutants[cityInfo.pollutants.length - 1].value,
      description: cityInfo.stationName,
      cursor: JSON.stringify(cityInfo),
      metrics: cityInfo.pollutants.map(metric => ({
        name: metric.pol,
        unit: metric.unit,
        dateTime: metric.time,
        value: metric.value,
        averaging: metric.averaging,
      })),
    })) : [];
  }
}

module.exports = BreathlyAPI;