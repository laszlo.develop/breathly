const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type CityAirInfo {
    id: ID!,
    name: String!,
    airQualityIndex: Int,
    description: String,
    cursor: String,
    metrics: [Metric]!,
  }
  
  type Metric {
    name: String!,
    unit: String!,
    dateTime: String!,
    value: Float,
    averaging: String!,
  }
  
  type qualitiesAirPagination {
    cursor: String!,
    hasMore: Boolean!,
    qualitiesOfAir: [CityAirInfo],
  }

  type Query {
    getQualitiesOfAir(
      """
      The number of results to show. Must be >= 1. Default = 20
      """
      pageSize: Int
      """
      If you add a cursor here, it will only return results _after_ this cursor
      """
      after: String
    ): qualitiesAirPagination!
    supportedCities: [String]
  }
`;

module.exports = typeDefs;