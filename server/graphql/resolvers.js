const { paginateResults } = require('../helpers');

module.exports = {
  Query: {
    supportedCities: (_, __, { dataSources }) =>
       dataSources.breathlyAPI.getAllSupportedCities(),
    getQualitiesOfAir: async (_, { pageSize = 20, after }, { dataSources }, info) => {
      info.cacheControl.setCacheHint({ maxAge: 50, scope: 'PRIVATE' });
      const allQualitiesOfAirData = await dataSources.breathlyAPI.getCitesInfo();
      const qualitiesOfAir = paginateResults({
        after,
        pageSize,
        results: allQualitiesOfAirData,
        getCursor: (item) => JSON.stringify(item),
      });
      return {
        qualitiesOfAir,
        cursor: qualitiesOfAir.length ? qualitiesOfAir[qualitiesOfAir.length - 1].cursor : null,
        hasMore: qualitiesOfAir.length
          ? qualitiesOfAir[qualitiesOfAir.length - 1].cursor !==
            allQualitiesOfAirData[allQualitiesOfAirData.length - 1].cursor
          : false
      };
    }
  }
};