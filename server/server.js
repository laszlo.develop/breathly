const express = require('express');
const Sequelize = require('sequelize');
const { ApolloServer } = require('apollo-server-express');

const typeDefs = require('./graphql/shema');
const resolvers = require('./graphql/resolvers');
const BreathlyAPI = require('./api/breathly');

const port = 4000;
const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    breathlyAPI: new BreathlyAPI(),
  }),
});
const backend = express();
server.applyMiddleware({ app: backend });

backend.listen({ port }, () => {
  console.log(`Server ready at http://localhost:4000${server.graphqlPath}`)
});

const sequelize = new Sequelize(process.env.POSTGRES_DB, process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'postgres',
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
