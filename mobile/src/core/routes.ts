export default {
  LOADING: 'LOADING',
  AUTH_LOADING: 'AUTH_LOADING',

  APP_STACK: 'App Stack',
  TEST_STACK: 'Test stack',
  APP: 'App',
};