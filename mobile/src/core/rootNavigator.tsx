import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { TestAppStack } from '../modules/app/routes';
import { Home } from '../modules/app/screens/home';
import ROUTES from './routes';

const AppStack = createBottomTabNavigator({
  [ROUTES.TEST_STACK]: {
    screen: TestAppStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <View>
          <Text>Hello world</Text>
        </View>
      ),
    },
  },
});

export default createAppContainer(
  createSwitchNavigator(
    {
      [ROUTES.APP_STACK]: {
        screen: Home,
      },
    },
    {
      initialRouteName: ROUTES.APP_STACK,
    },
  ),
);
