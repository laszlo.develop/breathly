import React from 'react';
import 'react-native-gesture-handler';
import * as eva from '@eva-design/eva';
import {
  ApplicationProvider,
  Layout,
  IconRegistry,
} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

import navigationService from './services/navigationService';
import RootNavigator from './core/rootNavigator';

const App: React.FC = () => {
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <Layout
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <RootNavigator
            ref={(navigatorRef) => {
              navigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Layout>
      </ApplicationProvider>
    </>
  );
};

export default App;
