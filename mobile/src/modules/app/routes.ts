import { createStackNavigator } from 'react-navigation-stack';
import ROUTES from '../../core/routes';

import { Home } from './screens/home';

export const TestAppStack = createStackNavigator(
  {
    [ROUTES.APP]: {
      screen: Home,
    },
  },
  {
    headerMode: 'none',
  },
);
