**To run a project:**

`yarn` -> `cd ios/pod install` -> `react-native run-ios`
`docker-compose up -d`

**Backend api available at:**

`http://localhost:4000/` -> `And GraphQl playground http://localhost:4000/graphql`
